# GAME CONCEPT DARK APOCALYPSE
**Genre :** Action aventure

**Moteur :** Unity 5

**Nombre de joueur :**

**Cible :** 12+

**Plateforme :** PC (windows/linux), mobile?

**Pitch :**
1 dash (ou un moyen d’aller vite) permet d’esquiver, dasher et attaquer la cible de l’ennemi. Faut casser toutes les cibles pour se débarrasser d’un ennemi. Les méchants ont leur cible dans leur dos (points faible ?)

**Univers :**
Décors délabrés style punk. Les ennemis rencontrés sont des aliens et/ou des humains  et/ou des robots. (Les scientifiques ayant eu recours à des bizarreries sur la maman et l’enfant, on peut imaginer qu’ils se soient modifiés le corps etc.)

## GAMEPLAY
**3C :** Caméra top view

**Camera :** 3D

**Control :** Manette

**Character :** On joue une jeune fille de couleur qui part se venger des scientifiques ayant fait du mal à sa mère adoptive.

**But du jeu :** Vaincre les ennemis rencontrés

**Condition de défaite :**  Se faire capturer

### DÉROULEMENT
**Ennemi 1 :** Vulnérable uniquement de dos

**Ennemi 2 :** La cible doit être frapper deux fois

**Boss :** Plusieurs point vulnérables à définir

## Idée de mécaniques :
Jauges fléchées au sol qui se charge en dashant dessus dans la même direction

Animation du dash : Le personnage lévite et s’envoit lui même dans une direction en boule (design en boule qui doit représenter son côté offensif) ou entouré d’une sphère de pouvoir.

Accumulation de charges pour dasher

\[pertinent ou pas ?\] Deux utilisation possible des charges, dash agressif, dash défensif. Le premier permet de frapper une cible, le second permet de se rendre intouchable pendant le dash.

## Références :
**Animations :** Gravity Rush

**Ambiance :** Creature In The Well

**Décors :** Sergi Brosa, Borderlands, FF VII 

**Personnages :** 
 - **Concept :** kakar cheung https://www.artstation.com/liluuu
 - **3D Model :** Christoph (The Stoff) Schoch  https://sketchfab.com/3d-models/geometric-tracer-94bdeb04193241919befeab96517c724
