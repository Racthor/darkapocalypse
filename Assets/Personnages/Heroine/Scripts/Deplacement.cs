﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deplacement : MonoBehaviour
{
    private CharacterController controller;
    public float speed = 5f; // units per second
    public float turnSpeed = 90f; // degrees per second
    public float jumpSpeed = 8f;
    public float gravity = 9.8f;
    private float vSpeed = 0f; // current vertical velocity

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    /*void FixedUpdate()
    {
        float depVertical = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        float depHorizontal = Input.GetAxis("Horizontal") * speed * Time.deltaTime;

        controller.SimpleMove(new Vector3(depHorizontal, 0, depVertical));
        if (Input.GetKeyDown(KeyCode.E))
        {
            competenceDash(controller);
        }
    }*/

    void Update()
    {
        transform.Rotate(0, Input.GetAxis("Horizontal") * turnSpeed * Time.deltaTime, 0);
        Vector3 vel = transform.forward * Input.GetAxis("Vertical") * speed;
        if (controller.isGrounded)
        {
            vSpeed = 0; // grounded character has vSpeed = 0...
            if (Input.GetKeyDown("space"))
            { // unless it jumps:
                vSpeed = jumpSpeed;
            }
        }
        // apply gravity acceleration to vertical speed:
        vSpeed -= gravity * Time.deltaTime;
        vel.y = vSpeed; // include vertical speed in vel
                        // convert vel to displacement and Move the character:
        controller.Move(vel * Time.deltaTime);
    }

    void competenceDash(CharacterController persoKiDash)
    {
        float distanceDeDash = 100;
        float depVertical = Input.GetAxis("Horizontal") * distanceDeDash;
        float depHorizontal = Input.GetAxis("Vertical") * distanceDeDash;
        persoKiDash.Move(new Vector3(depHorizontal, 0, depVertical));
    }
}